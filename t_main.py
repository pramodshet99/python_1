"""
this is the file which demonstrates module of time table.
"""
import t_generate as g
import t_display as dis

def take_input(sections = 5, hours_day = 4, days_week = 5, subject_count = 5, hours_subject = 4, teachers_hours = 3):
    """
    this function take inputs of subject and teacher
    
    """
    if hours_day * days_week > subject_count * hours_subject:
        hours_days = subject_count * hours_subject/days_week
    elif hours_day * days_week < subject_count * hours_subject:
        hours_subject = hours_day * days_week/subject_count
        
    return (sections, hours_day, days_week, subject_count, hours_subject, teachers_hours)    


if __name__ == "__main__":
    (sections, hours_day, days_week, subject_count, hours_subject, teachers_hours) = take_input()
    
    generate = g.generate_time_table(sections, hours_day, days_week, subject_count, hours_subject, teachers_hours)

    dis.display_section_timetable(days_week,subject_count,hours_subject)
    dis.display_teacher_timetable(days_week,subject_count,hours_subject,generate)
	
    



