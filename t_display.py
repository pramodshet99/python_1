import t_generate as g
day=["mon","tue","wed","thr","fri","sat","sun"]
def display_teacher_timetable(days_week,subject_count,hours_subject,sec_dict):
	"""
	this function display teachers timetable 
	
	"""
	print()
	print()
	print("TIME TABLE FOR EACH TEACHER SECTIONWISE")
	
	t_time_table=g.teacher_time_table(days_week,subject_count,hours_subject,sec_dict)
	j = 0
	
	for i in t_time_table:
		print()
		print('    section'+str(j+1))
		j += 1
		print()
		#print("  8-9am","9-10am","11-12am","12-1pm")
		d=0
		for k in i:
			print()
			print(day[d],k)
			d=(d+1)%7
	print()


def display_section_timetable(days_week,subject_count,hours_subject):
	"""
	this function display subject timetable for all sections
	
	"""
	print()
	print("   TIME TABLE FOR EACH SECTION")
	#print("  8-9am "," 9-10am "," 11-12am "," 12-1pm")
	s_time_table=g.section_time_table(days_week,subject_count,hours_subject)
	d=0
	for i in s_time_table:
		
		print(day[d],i)
		print()
		d=(d+1)%7
