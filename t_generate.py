"""
this is the file which demonstrates generation of time table.
"""
def teacher_time_table(days_week,subject_count,hours_subject,sec_dict):
	"""
	This function generates time table for each teacher
	"""
	n_time_table=section_time_table(days_week,subject_count,hours_subject)
	t_time_table = []
	for d in sec_dict:
		tm = []
		for i in range(days_week):
			j = 0
			tmp = []
			while j < hours_subject:
				for t in sec_dict[d]:
					if n_time_table[i][j] in sec_dict[d][t]:
						tmp.append(t)

				j += 1
			tm.append(tmp)
		t_time_table.append(tm)

	return t_time_table


def section_time_table(days_week,subject_count,hours_subject):
	"""
	This function generates time table for each section
	"""
	k = 1
	n_time_table = []
	for i in range(days_week):
		j = 0
		tmp = []
		while j < hours_subject:
			tmp.append('sub'+str(k%6))
			j += 1
			k += 1
			if k > subject_count :
				k = 1

		if k > subject_count:
			k = 1
		n_time_table.append(tmp)

	return n_time_table


def generate_time_table(sections, hours_day, days_week, subject_count, hours_subject, teachers_hours):
    """
    Allocation of subject per teacher
    
    """
    sub_for_teachers = 0
    n_of_teachers = 0
    sec_dict = {}
    
    for i in range(sections):
        k = 1
        sec_dict['section'+str(i+1)] = {}
        while k <= subject_count:
            if sub_for_teachers == 0:
                n_of_teachers += 1
            sec_dict['section'+str(i+1)]['T'+str(n_of_teachers)]=[]
       
      
            while sub_for_teachers < teachers_hours and k <= subject_count:
                sec_dict['section'+str(i+1)]['T'+str(n_of_teachers)].append('sub'+str(k%6))
                sub_for_teachers += 1
                k += 1
            if sub_for_teachers >= teachers_hours:
                sub_for_teachers=0
                
		
    return sec_dict